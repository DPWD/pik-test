import styled, { css } from 'styled-components';

export const Form = styled.form`
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
    max-width: 544px;
    min-width: 544px;
    background-color: #fff;
    border-radius: 8px;
    padding: 56px 56px 32px;
    @media (max-width: 768px) {
        max-width: none;
        min-width: auto;
        padding: 0;
    }
`;

export const InputGroup = styled.div`
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    gap: 32px;
    width: 100%;
    @media (max-width: 768px) {
        grid-template-columns: 1fr;
        gap: 0;
    }
`;

export const InputWrap = styled.div`
    position: relative;
    width: 100%;
    margin-bottom: 24px;
`;

export const Label = styled.label`
    position: absolute;
    left: 16px;
    top: 50%;
    transform: translateY(-50%);
    font-size: 16px;
    color: #4d4d4d;
    transition: all 0.2s linear;
    pointer-events: none;
`;

export const Input = styled.input<{ $error?: boolean; $isEmpty?: boolean }>`
    width: 100%;
    height: 56px;
    padding-top: 10px;
    padding-left: 16px;
    background-color: #f2f4f7;
    border: 2px solid #f2f4f7;
    border-radius: 4px;
    font-weight: 400;
    font-size: 16px;
    color: #4d4d4d;
    &:focus + label {
        font-size: 12px;
        color: #969ba5;
        top: 25%;
    }
    ${(prop) =>
        prop.$error &&
        css`
            background-color: rgba(230, 70, 70, 0.1);
            border: 2px solid #e64646;
            & + label {
                color: #e64646;
            }
        `}
    ${(prop) =>
        prop.$isEmpty &&
        css`
            & + label {
                font-size: 12px;
                color: #969ba5;
                top: 25%;
            }
        `}
`;

export const Error = styled.p`
    position: absolute;
    right: 0;
    bottom: -25%;
    font-weight: 400;
    font-size: 11px;
    color: #e64646;
`;

export const ResultWrap = styled.div`
    position: absolute;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    padding: 56px 75px 32px;
    background: #ffffff;
    border-radius: 8px;
    z-index: 1;
`;

export const IconWrap = styled.div`
    display: flex;
    margin-bottom: 34px;
`

export const ResultMessage = styled.p`
    font-style: normal;
    font-weight: 600;
    font-size: 44px;
    text-align: center;
    color: #000000;
    @media (max-width: 768px) {
        font-size: 36px;
    }
`;
