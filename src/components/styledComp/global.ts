import {createGlobalStyle, css} from 'styled-components';

export const GlobalStyle = createGlobalStyle<{$night?: boolean}>`
    body {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
        height: 100vh;
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
            'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
            sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        background: url('https://0.pik.ru.cdn.pik-service.ru/undefined/2021/08/03/dji_0093.rev00_wj16guVhKoupGK8K.jpg'); 
        ${props => props.$night && css`
            background: url('https://0.pik.ru.cdn.pik-service.ru/undefined/2020/07/21/dsc06845_481909dfb262bfdcb554e38bd110c38f_eZGKKhSFQDqht6yz.jpg'); 
        `}
        @media (max-width: 768px) {
            padding: 0 5%;
            background: #ffffff;
        }
    }
    * {
        box-sizing: border-box;
    }
    
    p {
        margin: 0;
    }
`