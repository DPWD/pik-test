import BookingForm from '../form/BookingForm';
import { GlobalStyle } from '../styledComp/global';

function App() {
    const isNight = () => {
        const hours: number = new Date().getHours();
        if (hours > 6 && hours < 18) {
            return false;
        }
        return true;
    };
    return (
        <>
            <GlobalStyle $night={isNight()} />
            <BookingForm />
        </>
    );
}

export default App;
