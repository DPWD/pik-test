import { SuccessIcon, ErrorIcon } from '../../resources/svg-icons';
import { ResultWrap, IconWrap, ResultMessage } from '../styledComp/form';

interface Props {
    formStatus: string;
}

const FormResult = ({ formStatus }: Props) => {
    if (formStatus !== 'success' && formStatus !== 'error') {
        return null;
    }
    const getMessage = () => {
        if (formStatus === 'success') {
            return 'Ваша заявка отправлена';
        }
        return 'Ошибка. Попробуйте позже';
    };
    return (
        <ResultWrap>
            <IconWrap>
                {formStatus === 'success' ? <SuccessIcon /> : <ErrorIcon />}
            </IconWrap>
            <ResultMessage>{getMessage()}</ResultMessage>
        </ResultWrap>
    );
};

export default FormResult;
