import styled from 'styled-components';

export const Title = styled.h1`
    font-weight: 600;
    font-size: 44px;
    margin: 0 0 16px;
    @media (max-width: 768px) {
        font-size: 32px;
    }
`;

export const Subtitle = styled.p`
    max-width: 314px;
    font-weight: 400;
    font-size: 16px;
    margin-bottom: 32px;
    text-align: center;
`;

export const Disclaimer = styled.p`
    font-weight: 400;
    font-size: 12px;
    color: #969BA5;
`;
