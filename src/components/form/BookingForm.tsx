import { ChangeEvent, InputHTMLAttributes, useEffect, useState } from 'react';
import { useFormik } from 'formik';
import { useHttp } from '../../hooks/http.hooks';
import * as Yup from 'yup';
//@ts-ignore
import InputMask from 'react-input-mask';
import ReactLoading from 'react-loading';

import FormResult from './FormResult';
import ErrorInput from './ErrorInput';

import { Form, InputGroup, Input, InputWrap, Label } from '../styledComp/form';
import { Title, Subtitle, Disclaimer } from '../styledComp/text';
import { Button } from '../styledComp/button';

interface FormData {
    firstName: string;
    lastname: string;
    phone: string;
    mail: string;
    flatsCount: string;
}

function BookingForm() {
    const [title, setTitle] = useState('');
    const [buttonText, setButtonText] = useState('Забронировать помещение');
    const [formStatus, setFormStatus] = useState('idle');

    useEffect(() => {
        setTitleDeclination();
    }, []);

    const { request } = useHttp();

    const formik = useFormik<FormData>({
        initialValues: {
            firstName: '',
            lastname: '',
            phone: '',
            mail: '',
            flatsCount: '',
        },
        validationSchema: Yup.object().shape({
            firstName: Yup.string().required('Заполните поле'),
            lastname: Yup.string().required('Заполните поле'),
            phone: Yup.string().required('Заполните поле'),
            mail: Yup.string()
                .email('Введите корректный email')
                .required('Заполните поле'),
            flatsCount: Yup.number().positive('Количество помещений должно быть больше 0').required('Заполните поле'),
        }),
        onSubmit: (values) => {
            setFormStatus('loading');
            const { firstName, lastname, phone, mail, flatsCount } = values;
            const time = Date.now();
            const sendData = {
                user: { firstName, lastname, phone, mail },
                order: { flatsCount, time },
            };
            request(
                'https://strapi.pik.ru/front-tests',
                'POST',
                JSON.stringify(sendData)
            )
                .then(() => setFormStatus('success'))
                .catch((e) => {
                    setFormStatus('error');
                    console.error(e);
                });
        },
    });

    const setTitleDeclination = () => {
        const date: number = new Date().getHours();
        if (date >= 0 && date < 6) {
            setTitle('Доброй ночи');
        } else if (date >= 6 && date < 12) {
            setTitle('Доброе утро');
        } else if (date >= 12 && date < 18) {
            setTitle('Добрый день');
        } else if (date >= 18 && date < 24) {
            setTitle('Добрый вечер');
        }
    };

    const onChangeInput = (e: ChangeEvent<HTMLInputElement>) => {
        if (e.target.name === 'flatsCount') {
            setButtonDeclination(parseInt(e.target.value, 10));
        }
        formik.handleChange(e);
    };

    const setButtonDeclination = (flatsCount: number) => {
        if (flatsCount <= 1) {
            setButtonText(`Забронировать помещение`);
        } else if (flatsCount > 1 && flatsCount < 5) {
            setButtonText(`Забронировать ${flatsCount} помещения`);
        } else if (flatsCount >= 5) {
            setButtonText(`Забронировать ${flatsCount} помещений`);
        }
    };

    return (
        <Form onSubmit={formik.handleSubmit}>
            <Title>{title}</Title>
            <Subtitle>Для бронирования помещений заполните форму</Subtitle>
            <InputGroup>
                <InputWrap>
                    <Input
                        type='text'
                        name='firstName'
                        onChange={onChangeInput}
                        value={formik.values.firstName}
                        $error={
                            !!formik.errors.firstName &&
                            !!formik.touched.firstName
                        }
                        $isEmpty={!!formik.values.firstName}
                    />
                    <Label htmlFor='firstName'>Ваше имя</Label>
                    <ErrorInput
                        error={formik.errors.firstName}
                        touched={formik.touched.firstName}
                    />
                </InputWrap>
                <InputWrap>
                    <Input
                        type='text'
                        name='lastname'
                        onChange={onChangeInput}
                        value={formik.values.lastname}
                        $error={
                            !!formik.errors.lastname &&
                            !!formik.touched.lastname
                        }
                        $isEmpty={!!formik.values.lastname}
                    />
                    <Label htmlFor='lastname'>Фамилия</Label>
                    <ErrorInput
                        error={formik.errors.lastname}
                        touched={formik.touched.lastname}
                    />
                </InputWrap>
            </InputGroup>
            <InputWrap>
                <InputMask
                    mask='+7 (999) 999-99-99'
                    maskChar=''
                    onChange={onChangeInput}
                    value={formik.values.phone}
                    $error={!!formik.errors.phone && !!formik.touched.phone}
                    $isEmpty={!!formik.values.phone}
                >
                    {(props: InputHTMLAttributes<HTMLInputElement>) => (
                        <Input type='phone' name='phone' {...props} />
                    )}
                </InputMask>
                <Label htmlFor='phone'>Телефон</Label>
                <ErrorInput
                    error={formik.errors.phone}
                    touched={formik.touched.phone}
                />
            </InputWrap>
            <InputWrap>
                <Input
                    type='email'
                    name='mail'
                    onChange={onChangeInput}
                    value={formik.values.mail}
                    $error={!!formik.errors.mail && !!formik.touched.mail}
                    $isEmpty={!!formik.values.mail}
                />
                <Label htmlFor='mail'>E-mail</Label>
                <ErrorInput
                    error={formik.errors.mail}
                    touched={formik.touched.mail}
                />
            </InputWrap>
            <InputWrap>
                <Input
                    type='number'
                    name='flatsCount'
                    onChange={onChangeInput}
                    value={formik.values.flatsCount}
                    $error={
                        !!formik.errors.flatsCount &&
                        !!formik.touched.flatsCount
                    }
                    $isEmpty={!!formik.values.flatsCount}
                />
                <Label htmlFor='flatsCount'>
                    Количество помещений {formik.isSubmitting}
                </Label>
                <ErrorInput
                    error={formik.errors.flatsCount}
                    touched={formik.touched.flatsCount}
                />
            </InputWrap>
            <Button type='submit'>
                {formStatus === 'loading' ? (
                    <ReactLoading
                        type='balls'
                        color='#ffffff'
                        height={32}
                        width={32}
                    />
                ) : (
                    buttonText
                )}
            </Button>
            <Disclaimer>Это дисклеймер, который есть во всех формах</Disclaimer>
            <FormResult formStatus={formStatus} />
        </Form>
    );
}

export default BookingForm;
