import { Error } from '../styledComp/form';

interface Props {
    error: string | undefined;
    touched: boolean | undefined;
}

const ErrorInput = ({ error, touched }: Props) => {
    if (error && touched) {
        return <Error>{error}</Error>;
    }
    return null;
};

export default ErrorInput
