import styled from 'styled-components';

export const Button = styled.button`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 56px;
    margin-bottom: 24px;
    background-color: #ff4114;
    border-radius: 4px;
    border: 1px solid transparent;
    font-weight: 400;
    font-size: 16px;
    color: #ffffff;
    cursor: pointer;
    transition: background-color 100ms linear, color 100ms linear,
        border 100ms linear;
    &:hover {
        background-color: #f72f00;
    }
    &:disabled {
        background-color: #ffffff;
        color: #cccccc;
        border: 1px solid #cccccc;
    }
    &:active {
        background-color: #dc2a00;
    }
`;
